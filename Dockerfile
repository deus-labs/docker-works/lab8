FROM adoptopenjdk/openjdk11:x86_64-alpine-jre-11.0.19_7
WORKDIR /lab8
COPY build/libs/*.jar lab8.jar
CMD [ "java", "-jar", "lab8.jar" ]